import Home from "./pages/home";
import "./App.css";

function App() {
  const webName = "Ariawan Web";
  return (
    <div className="App">
      <Home webName={webName} />
    </div>
  );
}

export default App;
