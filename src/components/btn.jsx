import React, { Component } from "react";

class Btn extends Component {
  state = {
    id: this.props.id,
    btn: this.props.btn,
    btnNmae: this.props.btnName,
    style: this.props.style,
    type: this.props.type,
  };
  render() {
    if (this.state.type == "Click") {
      return (
        <div className="col">
          <button
            id={this.state.id}
            onClick={this.state.btn}
            className={this.state.style}
          >
            {this.state.btnNmae}
          </button>
        </div>
      );
    } else if (this.state.type == "PopUp") {
      return (
        <div className="col">
          <button
            className={this.props.style}
            data-bs-toggle="modal"
            data-bs-target={"#viewsImage" + this.state.id}
          >
            {this.state.btnNmae}
          </button>
        </div>
      );
    }
  }
}

export default Btn;
