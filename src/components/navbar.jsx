import React, { Component } from "react";

class Navbar extends Component {
  state = {
    webName: this.props.webName,
  };
  render() {
    return (
      <React.Fragment>
        <nav className="navbar bg-light">
          <div className="container-fluid">
            <a className="navbar-brand">{this.state.webName}</a>
            <form className="d-flex" role="search">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button className="btn btn-outline-success" type="submit">
                Search
              </button>
            </form>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}

export default Navbar;
