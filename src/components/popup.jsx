import React, { Component } from "react";

class PopUp extends Component {
  state = {
    id: this.props.id,
    title: this.props.title,
    imgSrc: this.props.imgSrc,
    imgAlt: this.props.imgAlt,
  };
  render() {
    return (
      <React.Fragment>
        <div
          class="modal fade"
          id={"viewsImage" + this.state.id}
          tabindex="-1"
          aria-labelledby={"viewsImage" + this.state.id}
          aria-hidden="true"
        >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  {"Image " + this.state.title}
                </h5>
                <button
                  type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div class="modal-body">
                <img
                  src={this.state.imgSrc}
                  className="card-img-top"
                  alt={this.state.imgAlt}
                />
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default PopUp;
