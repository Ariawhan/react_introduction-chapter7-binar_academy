import React, { Component } from "react";

class Banner extends Component {
  state = { name: this.props.name };
  render() {
    return (
      <div class="alert alert-primary mt-3" role="alert">
        {this.state.name}
      </div>
    );
  }
}

export default Banner;
