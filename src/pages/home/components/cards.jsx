import React, { Component } from "react";
import Btn from "../../../components/btn";
import PopUp from "../../../components/popup";

class Cards extends Component {
  state = {
    id: this.props.id,
    title: this.props.title,
    description: this.props.description,
    imgSrc: this.props.imgSrc,
    imgAlt: this.props.imgAlt,
    btnText: this.props.btnText,
    btnHref: this.props.btnHref,
    onDelete: this.props.onDelete,
  };

  render() {
    return (
      <div className="col">
        <div className="card" style={{ width: "18rem" }}>
          <img
            src={this.state.imgSrc}
            className="card-img-top"
            alt={this.state.imgAlt}
          />
          <div className="card-body">
            <h5 className="card-title">{this.state.title}</h5>
            <p className="card-text">{this.state.description}</p>
            <div className="row">
              <Btn
                type="Click"
                btn={() => this.state.onDelete(this.state.id)}
                btnName="Delete"
                style="btn btn-danger"
                id={this.state.id}
              />
              <Btn
                type="PopUp"
                btnName="View"
                style="btn btn-primary"
                id={this.state.id}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Cards;
