import React, { Component } from "react";
import Navbar from "../../components/navbar";
import Cards from "./components/cards";
import Banner from "./components/banner";
import cardsData from "../../data/cards.json";
import PopUp from "../../components/popup";

class Home extends Component {
  state = {
    webName: this.props.webName,
    cardsData: cardsData,
  };

  handelDalete = (cardsId) => {
    const data = [];
    for (let i = 0; i < this.state.cardsData.length; i++) {
      if (this.state.cardsData[i].id !== cardsId) {
        data.push(this.state.cardsData[i]);
      }
    }
    this.setState({ cardsData: data });
    console.log(data);
  };

  render() {
    return (
      <div className="container">
        <Navbar webName={this.state.webName} />
        <Banner name="Welcome To cards" />
        {this.state.cardsData.map((cards) => (
          <PopUp
            id={cards.id}
            title={cards.title}
            imgSrc={cards.imgSrc}
            imgAlt={cards.imgAlt}
          />
        ))}
        <div className="row row-cols-1 row-cols-md-4 g-4">
          {this.state.cardsData.map((cards) => (
            <Cards
              id={cards.id}
              title={cards.title}
              description={cards.description}
              imgSrc={cards.imgSrc}
              imgAlt={cards.imgAlt}
              btnText={cards.btnText}
              btnHref={cards.btnHref}
              onDelete={this.handelDalete}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Home;
