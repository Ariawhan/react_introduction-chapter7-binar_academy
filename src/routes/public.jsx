import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "../pages/home/";

class Public extends Component {
  render() {
    return (
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    );
  }
}

export default Public;
